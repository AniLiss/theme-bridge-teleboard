# Branch 

Theme for Zendesk Help Center and Community

## Make development changes

```sh
$ npm install
$ gulp 
```

This will run gulp `connect` and `watch` tasks for development purposes.

## Make a build for sale

```sh
$ gulp build 
```

This command will create a production-ready version of the theme. You can find it in `build` directory.
