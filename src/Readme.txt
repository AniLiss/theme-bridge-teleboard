Bridge Theme for Freshdesk Support Center
===

About: http://www.breezythemes.com/products/freshdesk-theme-bridge 
Demo Support Center: https://bridge.breezythemes.com
Installation Guide: https://breezythemes.freshdesk.com/support/solutions/articles/2100022486-how-to-install-a-theme-
Refund Policy: https://checkout.shopify.com/16702687/policies/30377931.html

© Breezy Themes